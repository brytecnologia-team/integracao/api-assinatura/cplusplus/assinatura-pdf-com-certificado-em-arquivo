#include "curl/GetDownloadCurl.h"

GetDownloadCurl::GetDownloadCurl() : Curl() {}

GetDownloadCurl::~GetDownloadCurl()
{
    if (file)
        fclose(file);
}

void GetDownloadCurl::perform()
{
    if (file) {
        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, NULL);
	    curl_easy_setopt(curl, CURLOPT_WRITEDATA, file);
    } else {
        throw new CurlException("GetDownloadService: Não foi definido um arquivo para realizar o Download.");
    }

    Curl::perform();
}

void GetDownloadCurl::setFile(String filename)
{
    file = fopen(filename.c_str(), "wb");
}