#include <stdio.h>
#include <iostream>
#include <string>
#include <curl/curl.h>
#include <openssl/pem.h>
#include <openssl/err.h>
#include <openssl/pkcs12.h>
#include <openssl/sha.h>
#include <openssl/evp.h>
#include <json-c/json.h>

#include "config/certificate_config.h"
#include "config/signature_config.h"
#include "config/service_config.h"
#include "crypto/PKCS1DTO.h"
#include "crypto/signer.h"
#include "curl/GetDownloadCurl.h"
#include "curl/PostCurl.h"
#include "curl/Curl.h"
#include "util/util.h"
#include "util/file_util.h"
#include "util/http_util.h"
#include "util/get_url_from_json.h"
#include "service/JsonService.h"

#include "util/util_decode.h"

void check_variables();
PKCS1DTO * initialize_signature(Signer * signer);
void sign_content(Signer * signer, PKCS1DTO * pKCS1DTO_list);
std::string * finalize_signature(PKCS1DTO * pKCS1DTO_list);
std::string nonce = "";

int main(int argc, char ** argv)
{	
	//Step 0 - Verify global variables
	check_variables();

	// Step 1 - Load PrivateKey and Certificate
	Signer * signer = new Signer(PRIVATE_KEY_LOCATION, PRIVATE_KEY_PASSWORD);

	// Step 2 - Signature initialization.
	PKCS1DTO * pKCS1DTO_list = initialize_signature(signer);

	// Step 3 - Local encryption of signed attributes using private key.
	sign_content(signer, pKCS1DTO_list);	

	// Step 4 - Signature finalization.
	std::string * urls = finalize_signature(pKCS1DTO_list);

	// Step 5 - Write signatures to files.
	for (int i = 0; i < NUMBER_OF_DOCUMENTS; i++)
	{
		GetDownloadCurl getCurl;
		getCurl.setURL(urls[i]);
		getCurl.setFile(OUTPUT_RESOURCE_FOLDER "signature-item-" + std::to_string(i + 1) + ".pdf");
		getCurl.perform();
	}	

	delete signer;
	delete[] urls;
}

PKCS1DTO * initialize_signature(Signer * signer) {
	unsigned char * cert_b64 = cert_pem_to_base64(signer->get_certificate());

	JsonService jsonService;
	jsonService.addValor("perfil", PROFILE);
	jsonService.addValor("algoritmoHash", HASH_ALGORITHM);
	jsonService.addValor("formatoDadosEntrada", FORMAT_IN);
	jsonService.addValor("formatoDadosSaida", FORMAT_OUT);
	jsonService.addValor("certificado", std::string((const char *) cert_b64));	
	JsonArrayService jsonArray;
	for(int i = 0; i < NUMBER_OF_DOCUMENTS; i++)
	{
		jsonArray.addValor("\"" + std::to_string(i) + "\"");
	}
	jsonService.addArray("nonces", jsonArray.getString());
	std::string dados_inicializar = jsonService.getString();
	std::cout << "Post Dados Inicializar:" << dados_inicializar.c_str() << std::endl;

	//Dados de configuracao de imagem
	JsonService jsonImagem;
	jsonImagem.addValor("altura", "30");
	jsonImagem.addValor("largura", "100");
	jsonImagem.addValor("coordenadaY", "30");
	jsonImagem.addValor("coordenadaX", "30");
	jsonImagem.addValor("posicao", "INFERIOR_ESQUERDO");
	jsonImagem.addValor("proporcaoImagem", "30");
	std::string configuracao_imagem = jsonImagem.getString();
	std::cout << "Post configuracao Imagem:" << configuracao_imagem.c_str() << std::endl;

	//Dados de Configuracao de texto
	JsonService jsonTexto;
	jsonTexto.addValor("texto","Doc está assinado");
	jsonTexto.addValor("incluirCN","true");
	std::string configuracao_texto = jsonTexto.getString();
	std::cout << "Post configuracao Texto:" << configuracao_texto.c_str() << std::endl;

	PostCurl postCurl = PostCurl();
	postCurl.setURL(URL_INITIALIZE_SIGNATURE);
	postCurl.addHeader("Authorization", ACCESS_TOKEN);
	postCurl.addMime("dados_inicializar", dados_inicializar);
	postCurl.addMime("configuracao_texto", configuracao_texto);
	postCurl.addMime("configuracao_imagem", configuracao_imagem);
	postCurl.addFile("imagem", IMAGE_LOCATION);
	postCurl.addFile("imagemFundo", BACKGROUND_IMAGE_LOCATION);
	for(int i = 0; i < NUMBER_OF_DOCUMENTS; i++)
	{
		postCurl.addFile("documento", ORIGINAL_DOCUMENTS_LOCATION[i]);
	}
	postCurl.perform();

	long status_code = postCurl.getHttpCode();
	std::string * response_body = postCurl.getResponse();

	if(status_code != 200)
	{
		std::cout << "Error during signature initialization - Status code: " << status_code << std::endl;
		std::cout << response_body->c_str() << std::endl;
		exit(1);
	}

	std::cout << "Signature initialization JSON response: " << response_body->c_str() << std::endl;

	json_object * response_body_as_json = postCurl.getJsonResponse();

	json_object * signed_attributes_json_array;
	json_object_object_get_ex(response_body_as_json, "assinaturasInicializadas", &signed_attributes_json_array);

	PKCS1DTO * pKCS1DTO_list = new PKCS1DTO[NUMBER_OF_DOCUMENTS];

	for(int i = 0; i < NUMBER_OF_DOCUMENTS; i++)
	{
		json_object * signed_attributes_as_json = json_object_array_get_idx(signed_attributes_json_array, i);

		json_object * content_as_json;
		json_object_object_get_ex(signed_attributes_as_json, "messageDigest", &content_as_json);

		json_object * nonce_as_json;
		json_object_object_get_ex(signed_attributes_as_json, "nonce", &nonce_as_json);

		const char * nonce_as_string = json_object_get_string(nonce_as_json);
		const char * content_as_string = json_object_get_string(content_as_json);

		char * nonce = new char[strlen(nonce_as_string) + 1];
		strcpy(nonce, nonce_as_string);

		char * content = new char[strlen(content_as_string) + 1];
		strcpy(content, content_as_string);

		pKCS1DTO_list[i].set_nonce(nonce);
		pKCS1DTO_list[i].set_content(content);
		pKCS1DTO_list[i].set_file(ORIGINAL_DOCUMENTS_LOCATION[i].c_str());
	}

	json_object * nonce_json = json_object_object_get(response_body_as_json, "nonce");
	nonce = json_object_get_string(nonce_json);	

	delete[] cert_b64;
	json_object_put(response_body_as_json);

	return pKCS1DTO_list;
}

void sign_content(Signer * signer, PKCS1DTO * pKCS1DTO_list)
{
	for(int i = 0; i < NUMBER_OF_DOCUMENTS; i++)
	{
		std::string content = pKCS1DTO_list[i].get_content();
		std::string decoded_content = base64_decode(content);

		unsigned char * sig;
		size_t * s_len;
		signer->sign(HASH_ALGORITHM, decoded_content.c_str(), decoded_content.size(), &sig, &s_len);

		int input_length = (int) *s_len;

		std::string signature = (char *) sig;
		std::string encoded_signature = base64_encode( sig, input_length, false);

		pKCS1DTO_list[i].set_signature_string(encoded_signature);
	}
}

std::string * finalize_signature(PKCS1DTO * pKCS1DTO_list)
{
	JsonService jsonService;	
	jsonService.addValor("nonce", nonce);
	jsonService.addValor("formatoDeDados", "Base64");

	JsonArrayService jsonArray;
	for(int i = 0; i < NUMBER_OF_DOCUMENTS; i++)
	{
		JsonService arr;
		arr.addValor("nonce", pKCS1DTO_list[i].get_nonce());
		arr.addValor("cifrado", pKCS1DTO_list[i].get_signature_string());

		jsonArray.addValor(arr.getString());
	}
	jsonService.addArray("assinaturasPkcs1", jsonArray.getString());
	std::string json = jsonService.getString();

	std::cout << "Json de envio finalização: " << json.c_str() << std::endl;

	PostCurl postCurl = PostCurl();
	postCurl.addHeader("Content-Type", "application/json");
	postCurl.setJson(json);
	postCurl.setURL(URL_FINALIZE_SIGNATURE);
	postCurl.perform();

	long status_code = postCurl.getHttpCode();
	std::string * response_body = postCurl.getResponse();
	if(status_code != 200)
	{
		std::cout << "Error during signature finalization - Status code: " << status_code << std::endl;
		std::cout << response_body -> c_str() << std::endl;
		exit(1);
	}

	std::cout << "Signature finalization JSON response: " << response_body->c_str() << std::endl;

	json_object * response_body_as_json = postCurl.getJsonResponse();

	json_object * signatures_array_json;
	json_object_object_get_ex(response_body_as_json, "documentos", &signatures_array_json);

	std::string * signatures = new std::string[NUMBER_OF_DOCUMENTS];
	for(int i = 0; i < NUMBER_OF_DOCUMENTS; i++)
	{
		//Pega assinatura
		json_object * signature_as_json = json_object_array_get_idx(signatures_array_json, i);

		//Signatures[i] = url para download
		signatures[i] = get_url_from_json(signature_as_json);
	}

	json_object_put(response_body_as_json);
	return signatures;
}
void check_variables()
{
	//Verifica se informações estão preenchidas
	if (std::string(ACCESS_TOKEN).compare("<ACESS_TOKEN>") == 0) {
		std::cout << "Inserir um token de acesso válido" << std::endl;
		exit(1);
	} else if (std::string(PRIVATE_KEY_LOCATION).compare("<PRIVATE_KEY_LOCATION>") == 0) {
		std::cout << "Inserir o local onde está o certificado" << std::endl;
		exit(2);
	} else if (std::string(PRIVATE_KEY_PASSWORD).compare("<PRIVATE_KEY_PASSWORD>") == 0) {
		std::cout << "Inserir a senha do certificado" << std::endl;
		exit(3);
	}
}