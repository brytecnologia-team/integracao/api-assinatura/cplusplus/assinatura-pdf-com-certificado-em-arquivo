#include "service/JsonService.h"

void JsonService::addValor(String key, String value)
{
    if (dados.size() > 0)
        dados += ",\n";
    dados += "\"" + key;
    dados += "\" : \"" + value;
    dados += "\"";
}

std::string JsonService::getString()
{
    dados = "{\n" + dados;
    dados += "\n}";

    return dados;
}
void JsonService::addArray(String key, String value)
{
    if (dados.size() > 0)
        dados += ",\n";
    dados += "\"" + key;
    dados += "\" : " + value;
}