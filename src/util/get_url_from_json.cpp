#include "util/get_url_from_json.h"

void verifica_json(json_object * object) {
    if (!object) {
        throw new JsonFormatException();
    }
}

std::string get_url_from_json(json_object * object)
{
    verifica_json(object);

	//Pega json array de links
	json_object * content_as_json_array = json_object_object_get(object, "links");
    verifica_json(content_as_json_array);

	//Pega o primeiro link
	json_object * link_as_json = json_object_array_get_idx(content_as_json_array, 0);
    verifica_json(link_as_json);

	//Pega href
	json_object * content_as_json;
	json_object_object_get_ex(link_as_json, "href", &content_as_json);
    verifica_json(content_as_json);

    std::string retorno = json_object_get_string(content_as_json);

	return retorno;
}