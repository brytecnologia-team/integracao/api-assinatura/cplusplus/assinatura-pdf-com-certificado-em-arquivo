#include "util/util.h"

const char * remove_substring(const char * text, const char * to_remove)
{
	std::string _to_remove(to_remove);
	std::string * _text = new std::string(text);
	size_t pos = std::string::npos;

	while((pos = _text->find(_to_remove)) != std::string::npos)
		_text->erase(pos, _to_remove.length());

	return _text->c_str();
}

unsigned char * cert_pem_to_base64(const char * cert_pem)
{
	const char * tmp = remove_substring(cert_pem, BEGIN_CERTIFICATE);
	const char * tmp2 = remove_substring(tmp, END_CERTIFICATE);
	unsigned char * ret = (unsigned char *) remove_substring(tmp2, "\n");

	delete[] tmp;
	delete[] tmp2;

	return ret;
}
