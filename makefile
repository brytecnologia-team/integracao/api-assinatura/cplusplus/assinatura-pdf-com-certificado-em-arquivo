include ./makedefs

UTIL_SRC_FILES   = $(wildcard $(UTIL)/*.cpp)
UTIL_OBJ_FILES   = $(patsubst $(UTIL)/%.cpp, $(UTIL)/%.o, $(UTIL_SRC_FILES))
CURL_SRC_FILES   = $(wildcard $(CURL)/*.cpp)
CURL_OBJ_FILES   = $(patsubst $(CURL)/%.cpp, $(CURL)/%.o, $(CURL_SRC_FILES))
CRYPTO_SRC_FILES = $(wildcard $(CRYPTO)/*.cpp)
CRYPTO_OBJ_FILES = $(patsubst $(CRYPTO)/%.cpp, $(CRYPTO)/%.o, $(CRYPTO_SRC_FILES))
SERVICE_SRC_FILES   = $(wildcard $(SERVICE)/*.cpp)
SERVICE_OBJ_FILES   = $(patsubst $(SERVICE)/%.cpp, $(SERVICE)/%.o, $(SERVICE_SRC_FILES))
OBJ_FILES        = $(CRYPTO_OBJ_FILES) $(UTIL_OBJ_FILES) $(SERVICE_OBJ_FILES) $(CURL_OBJ_FILES)
	
all: basic_signature

basic_signature: $(OBJ_FILES) $(SRC)/basic_signature_example.o
	$(CXX) -o $@ $(OBJ_FILES) $(SRC)/basic_signature_example.o $(CXXFLAGS)
	
%.o: %.cpp
	$(CXX) -o $@ -c $< $(CXXFLAGS)

$(OBJ_FILES):
	$(MAKE) -C $(UTIL)
	$(MAKE) -C $(CURL)
	$(MAKE) -C $(CRYPTO)
	$(MAKE) -C $(SERVICE)

.PHONY: clean

clean:
	$(MAKE) -C $(CRYPTO) clean
	$(MAKE) -C $(UTIL) clean
	$(MAKE) -C $(SERVICE) clean
	$(MAKE) -C $(CURL) clean
	rm -f $(SRC)/*.o basic_signature core resources/generatedSignatures/* $(SRC)/*~ $(INCLUDE)/*~
	