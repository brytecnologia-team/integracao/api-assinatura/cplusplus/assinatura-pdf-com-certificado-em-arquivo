#ifndef __GET_DOWNLOAD_SERVICE_H
#define __GET_DOWNLOAD_SERVICE_H

#include "curl/Curl.h"
#include <stdio.h>

class GetDownloadCurl : public Curl {
typedef std::string String;

private:
    FILE * file;

public:
    GetDownloadCurl();
    ~GetDownloadCurl();
    void perform();
    void setFile(String filename);
};

#endif