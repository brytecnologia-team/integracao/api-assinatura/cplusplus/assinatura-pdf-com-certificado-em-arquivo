#ifndef __signature_config_h
#define __signature_config_h

#include <string>

#define HASH_ALGORITHM "SHA256"

#define PROFILE "ADRB"

#define FORMAT_IN "Base64"

#define FORMAT_OUT "Base64"

#define NUMBER_OF_DOCUMENTS 2

const std::string ORIGINAL_DOCUMENTS_LOCATION[] = {"./resources/documents/doc.pdf" , "./resources/documents/doc_copy.pdf"};

#define OUTPUT_RESOURCE_FOLDER "./resources/generatedSignatures/"

#define IMAGE_LOCATION "./resources/documents/imagem.jpg"

#define BACKGROUND_IMAGE_LOCATION "./resources/documents/imagemFundo.jpg"

#endif
