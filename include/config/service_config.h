#ifndef __service_config_h
#define __service_config_H

#define URL_SERVER "https://hub2.hom.bry.com.br"

#define URL_SERVER_PDF URL_SERVER "/fw/v1/pdf/pkcs1/assinaturas/acoes/"

#define URL_INITIALIZE_SIGNATURE URL_SERVER_PDF "inicializar"

#define URL_FINALIZE_SIGNATURE URL_SERVER_PDF "finalizar"

#define ACCESS_TOKEN "<ACESS_TOKEN>"

#endif
