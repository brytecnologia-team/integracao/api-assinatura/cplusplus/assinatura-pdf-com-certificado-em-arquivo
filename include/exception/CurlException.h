#ifndef __curl_service_exception_h
#define __curl_service_exception_h

#include <exception>
#include <string>

class CurlException: public std::exception
{
typedef std::string String;

private:
    String error_message;

public:
    CurlException(String msg) {
      error_message = "Um erro aconteceu ao tentar realizar com CURL: " + msg;
    }
    
    virtual const char* what() const throw()
    {
      return error_message.c_str();
    }
};

#endif