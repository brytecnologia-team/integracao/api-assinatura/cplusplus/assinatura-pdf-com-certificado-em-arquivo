#ifndef __json_format_exception_h
#define __json_format_exception_h

#include <exception>
#include <string>

class JsonFormatException: public std::exception
{
typedef std::string String;

private:
    String error_message;

public:
    JsonFormatException() {
      error_message = "Json format was not as expected.";
    }

    JsonFormatException(String msg) {
      error_message = "Json format was not as expected: " + msg;
    }
    
    virtual const char* what() const throw()
    {
      return error_message.c_str();
    }
};

#endif