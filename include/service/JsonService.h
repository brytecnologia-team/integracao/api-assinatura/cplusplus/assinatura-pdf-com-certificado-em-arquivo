#ifndef __JSON_SERVICE_H
#define __JSON_SERVICE_H

#include <string>
#include "JsonArrayService.h"

class JsonService {
typedef std::string String;

private:
    String dados = "";

public:
    void addValor(String key, String value);
    void addArray(String key, String value);
    String getString();
};

#endif