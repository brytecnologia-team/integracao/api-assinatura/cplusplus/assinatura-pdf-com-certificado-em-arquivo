#ifndef __signer_h
#define __signer_h

#include <openssl/pem.h>
#include <openssl/err.h>
#include <openssl/pkcs12.h>
#include <openssl/sha.h>
#include <openssl/evp.h>
#include <string.h>
#include <stdio.h>
#include <iostream>

class Signer
{

public:
	Signer(const char * keystore_path, const char * keystore_password);
	~Signer();

	char * get_certificate();
	void sign(const char * hash_algorithm, const char * data, int data_len, unsigned char ** sigret, size_t ** s_len_ret);

private:
	EVP_PKEY * priv_key;
	char * cert_pem;

};

#endif
